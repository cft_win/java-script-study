# DOM
## 基础知识
DOM【文档对象模型】Document Object Model

### 节点层次
```html
<html>
  <head>
    <title>Sample Page</title>
  </head>
  <body>
    <p>Hello World</p>
  </body>
</html>
```
> document节点表示每个文档的根节点,根节点的唯一子节点是`<html>`元素

#### Node类型
- Node.ELEMENT_NODE = 1; (最常用)
- Node.ATTRIBUTE_NODE = 2;
- Node.TEXT_NODE = 3; (最常用)
- Node.CDATA_SECTION_NODE = 4;
- Node.ENTITY_REFERENCE_NODE = 5;
- Node.ENTITY_NODE = 6;
- Node.PROCESSING_INSTRUCTION_NODE = 7;
- Node.COMMENT_NODE = 8;
- Node.DOCUMENT_NODE = 9;
- Node.DOCUMENT_TYPE_NODE = 10;
- Node.DOCUMENT_FRAGMENT_NODE = 11;
- Node.NOTATION_NODE = 12;

<h5>1. nodeName 和 nodeValue</h5>
> 表示元素的标签名, 当nodeType为ELEMENT_NODE时, 例如div的nodeName是DIV, nodeValue为null; 当nodeType为TEXT_NODE时, nodeName为#text, nodeValue为#text

<h5>2. 节点关系</h5>
<h6>childNodes</h6>
每个节点都有childNodes属性,是一个NodeList实例数组【注意: NodeList是一个类数组对象,用于存储可以按位置存取的有序节点】,如果想对NodeList访问,可以将NodeList转为数组或者使用索引的方式访问,当节点没有子节点时,childNodes属性为空NodeList类数组,NodeList类数组的length为0
- 访问NodeList类数组的方式
  - 转数组
  - 索引
  - item(index), index是索引从0开始

<h6>parentNode</h6>
> 每个节点都有parentNode属性,表示父节点,当节点没有父节点时,parentNode为null【document是根节点,所以document的parentNode为null】

<h6>previousSibling</h6>
> 表示该节点的上一个节点。如果是第一个节点,则previousSibling为null

<h6>nextSibling</h6>
> 表示该节点的下一个节点。如果是最后一个节点,则nextSibling为null

<h6>firstChild</h6>
> 表示该节点下的第一个子节点

<h6>lastChild</h6>
> 表示该节点下的最后一个子节点

<h6>hasChildNodes()</h6>
> 表示该节点是否有子节点

<h6>ownerDocument</h6>
> 表示代表整个文档的文档节点【document】

<h5>3.操纵节点</h5>
<h6>appendChild()</h6>
> 用于在childNodes列表结尾添加节点【添加新节点会更新相关的关系指针,包含父节点和之前的最后一个子节点】,appendChild()方法返回添加的节点

> 如果把文档中已存在的节点传递给appendChild()方法,则该节点会从原来的位置移除,并添加到新的位置,并返回该节点【会更新关系指针】

<h6>insertBefore()</h6>
> 将节点插入到指定位置【要插入的节点会变成参照节点的前一个兄弟节点,并返回】,如果参照节点为null,则insertBefore()与appendChild()效果相同

<h6>replaceChild()</h6>
> 用一个节点替换另一个节点;接收两个参数: 要插入的节点和要替换的节点【返回被替换的节点】

<h6>remoevChild()</h6>
> 移除节点,接收一个参数,即要移除的节点【返回被移除的节点】

<h5>4.其他方法</h5>
<h6>cloneNode()</h6>
> 表示返回与调用它的节点一模一样的节点。有一个参数布尔值【true 表示深复制, 深复制会复制节点及其整个子DOM树】,这个方法只复制HTML属性,以及可选地复制子节点,不会复制添加到DOM节点的JavaScript属性, 比如事件处理程序

#### Document类型
<h5>1.文档节点</h5>
- 我们要访问html元素对象,可以通过document.childNodes[0] 或 document.firstChild 或 document.documentElement 
- 我们要访问body元素对象,可以通过document.body

<h5>2.文档信息</h5>
- document.title 设置/获取浏览器窗口或标签页的标题
- document.URL 包含当前页面的完整URL(地址栏)
- document.domain 包含页面的域名
- document.referrer 包含链接到当前页面的那个页面的URL,如果当前页面没有来源,则referrer为空字符串

<h5>3.定位元素</h5>
- document.getElementById() 根据id获取元素 【如果找到则返回这个元素,如果没有找到则返回null(id区分大小写)】注意: 如果页面有多个相同的ID元素,则返回第一个
- document.getElementsByTagName() 根据标签名获取元素 【获取零个或多个元素的NodeList】
- HTMLCollection可以通过namedItem('xxx')获取元素【xxx是元素属性name的值】,也可以通过HTMLCollection["xxx"];中括号既可以接收数值索引,也可以接收字符串索引,而在后台数值索引会调用item(),字符串索引会调用namedItem()
- document.getElmentsByName() 根据name属性获取元素 【获取零个或多个元素的HTMLCollection】

<h5>4.特殊集合</h5>
- document.anchors 包含文档中所有带name属性的`</a>`元素
- document.forms 包含文档中所有`<form>`元素
- document.images 包含文档中所有`<img>`元素
- document.links 包含文档中所有带href属性的`<a>`元素

<h6>6.文档写入</h6>
- write() 传入的字符串写入网页中
- writeln() 传入的字符串写入网页中,还会在字符串结尾追加一个换行符
> write() 和 writeln()方法经常用于动态包含外部资源
